package com.dev.hygino.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dev.hygino.dto.MedicineDTO;
import com.dev.hygino.dto.MedicineInsertDTO;

import jakarta.validation.Valid;

public interface MedicineService {

	MedicineDTO insert(@Valid MedicineInsertDTO insertDTO);

	Page<MedicineDTO> findAll(Pageable pageable);

}
