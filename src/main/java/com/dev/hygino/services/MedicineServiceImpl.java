package com.dev.hygino.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dev.hygino.dto.MedicineDTO;
import com.dev.hygino.dto.MedicineInsertDTO;
import com.dev.hygino.entities.Laboratory;
import com.dev.hygino.entities.Medicine;
import com.dev.hygino.repositories.LaboratoryRepository;
import com.dev.hygino.repositories.MedicineRepository;

import jakarta.validation.Valid;

@Service
public class MedicineServiceImpl implements MedicineService {

	private final LaboratoryRepository laboratoryRepository;
	private final MedicineRepository medicineRepository;

	public MedicineServiceImpl(LaboratoryRepository laboratoryRepository, MedicineRepository medicineRepository) {
		this.laboratoryRepository = laboratoryRepository;
		this.medicineRepository = medicineRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<MedicineDTO> findAll(Pageable pageable) {
		Page<Medicine> page = medicineRepository.findAll(pageable);

		return page.map(med -> new MedicineDTO(med));
	}

	@Override
	@Transactional
	public MedicineDTO insert(@Valid MedicineInsertDTO dto) {
		Medicine entity = new Medicine();
		Laboratory laboratory = laboratoryRepository.getReferenceById(dto.laboratoryId());

		entity.setName(dto.name());
		entity.setVia(dto.via());
		entity.setLaboratory(laboratory);
		entity = medicineRepository.save(entity);

		return new MedicineDTO(entity);
	}

}
