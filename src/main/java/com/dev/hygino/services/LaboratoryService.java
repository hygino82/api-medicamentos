package com.dev.hygino.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dev.hygino.dto.LaboratoryDTO;
import com.dev.hygino.dto.LaboratoryInsertDTO;

import jakarta.validation.Valid;

public interface LaboratoryService {

    Page<LaboratoryDTO> findAll(Pageable pageable);

    LaboratoryDTO findById(Long id);

    LaboratoryDTO insert(@Valid LaboratoryInsertDTO insertDTO);

    LaboratoryDTO update(Long id, @Valid LaboratoryInsertDTO insertDTO);

    void delete(Long id);

}
