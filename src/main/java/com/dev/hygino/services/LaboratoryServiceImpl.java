package com.dev.hygino.services;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dev.hygino.dto.LaboratoryDTO;
import com.dev.hygino.dto.LaboratoryInsertDTO;
import com.dev.hygino.entities.Laboratory;
import com.dev.hygino.repositories.LaboratoryRepository;
import com.dev.hygino.services.exceptions.DatabaseException;
import com.dev.hygino.services.exceptions.ResourceNotFoundException;

import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class LaboratoryServiceImpl implements LaboratoryService {

	private final ModelMapper mapper;
	private final LaboratoryRepository laboratoryRepository;

	public LaboratoryServiceImpl(ModelMapper mapper, LaboratoryRepository laboratoryRepository) {
		this.mapper = mapper;
		this.laboratoryRepository = laboratoryRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<LaboratoryDTO> findAll(Pageable pageable) {
		Page<Laboratory> page = laboratoryRepository.findAll(pageable);

		return page.map(obj -> mapper.map(obj, LaboratoryDTO.class));
	}

	@Override
	@Transactional(readOnly = true)
	public LaboratoryDTO findById(Long id) {
		Optional<Laboratory> obj = laboratoryRepository.findById(id);
		Laboratory entity = obj.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));

		return mapper.map(entity, LaboratoryDTO.class);
	}

	@Override
	@Transactional
	public LaboratoryDTO insert(@Valid LaboratoryInsertDTO insertDTO) {
		Laboratory entity = new Laboratory();
		entity.setName(insertDTO.name());
		entity = laboratoryRepository.save(entity);

		return mapper.map(entity, LaboratoryDTO.class);
	}

	@Override
	@Transactional
	public LaboratoryDTO update(Long id, @Valid LaboratoryInsertDTO dto) {
		try {
			Laboratory entity = laboratoryRepository.getReferenceById(id);
			entity.setName(dto.name());
			entity = laboratoryRepository.save(entity);
			return mapper.map(entity, LaboratoryDTO.class);
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		}
	}

	@Override
	public void delete(Long id) {
		try {
			laboratoryRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		} catch (DataIntegrityViolationException e) {
			throw new DatabaseException("Integrity violation");
		}
	}
}
