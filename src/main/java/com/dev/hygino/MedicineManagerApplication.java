package com.dev.hygino;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicineManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicineManagerApplication.class, args);
	}
}
