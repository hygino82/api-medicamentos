package com.dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dev.hygino.dto.LaboratoryDTO;
import com.dev.hygino.dto.LaboratoryInsertDTO;
import com.dev.hygino.services.LaboratoryService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/laboratory")
public class LaboratoryController {

	private final LaboratoryService laboratoryService;

	public LaboratoryController(LaboratoryService laboratoryService) {
		this.laboratoryService = laboratoryService;
	}

	@GetMapping
	public ResponseEntity<Page<LaboratoryDTO>> findAll(Pageable pageable) {
		Page<LaboratoryDTO> pageDto = laboratoryService.findAll(pageable);

		return ResponseEntity.status(200).body(pageDto);
	}

	@GetMapping("/{id}")
	public ResponseEntity<LaboratoryDTO> findById(@PathVariable("id") Long id) {
		LaboratoryDTO dto = laboratoryService.findById(id);

		return ResponseEntity.status(200).body(dto);
	}

	@PostMapping
	public ResponseEntity<LaboratoryDTO> insert(@Valid @RequestBody LaboratoryInsertDTO insertDTO) {
		LaboratoryDTO dto = laboratoryService.insert(insertDTO);

		return ResponseEntity.status(201).body(dto);
	}

	@PutMapping("/{id}")
	public ResponseEntity<LaboratoryDTO> update(@PathVariable("id") Long id,
			@Valid @RequestBody LaboratoryInsertDTO insertDTO) {
		LaboratoryDTO dto = laboratoryService.update(id, insertDTO);

		return ResponseEntity.status(200).body(dto);
	}

	@DeleteMapping("/{id}")
	ResponseEntity<LaboratoryDTO> delete(@PathVariable("id") Long id) {
		laboratoryService.delete(id);

		return ResponseEntity.noContent().build();
	}
}
