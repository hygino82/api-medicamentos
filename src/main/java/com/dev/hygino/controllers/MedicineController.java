package com.dev.hygino.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dev.hygino.dto.MedicineDTO;
import com.dev.hygino.dto.MedicineInsertDTO;
import com.dev.hygino.services.MedicineService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/medicine")
public class MedicineController {

	private final MedicineService medicineService;

	public MedicineController(MedicineService medicineService) {
		this.medicineService = medicineService;
	}

	@GetMapping
	public ResponseEntity<Page<MedicineDTO>> findAll(Pageable pageable) {
		Page<MedicineDTO> page = medicineService.findAll(pageable);
		return ResponseEntity.status(200).body(page);
	}

	@PostMapping
	public ResponseEntity<MedicineDTO> insert(@Valid @RequestBody MedicineInsertDTO insertDTO) {
		MedicineDTO dto = medicineService.insert(insertDTO);
		return ResponseEntity.status(201).body(dto);
	}
}
