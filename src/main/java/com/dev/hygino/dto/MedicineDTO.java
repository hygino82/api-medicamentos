package com.dev.hygino.dto;

import com.dev.hygino.entities.Medicine;
import com.dev.hygino.entities.enums.Via;

public class MedicineDTO {
	
	private Long id;
	private String name;
	private String laboratoryName;
	private Via via;

	public MedicineDTO() {
	}

	public MedicineDTO(Medicine entity) {
		id = entity.getId();
		name = entity.getName();
		laboratoryName = entity.getLaboratory().getName();
		via = entity.getVia();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLaboratoryName() {
		return laboratoryName;
	}

	public void setLaboratoryName(String laboratoryName) {
		this.laboratoryName = laboratoryName;
	}

	public Via getVia() {
		return via;
	}

	public void setVia(Via via) {
		this.via = via;
	}
}
