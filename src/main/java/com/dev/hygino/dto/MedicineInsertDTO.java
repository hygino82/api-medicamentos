package com.dev.hygino.dto;

import com.dev.hygino.entities.enums.Via;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record MedicineInsertDTO(@NotBlank String name, @NotNull Long laboratoryId, @NotNull Via via) {

}