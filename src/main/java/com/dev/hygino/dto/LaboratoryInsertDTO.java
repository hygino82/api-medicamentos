package com.dev.hygino.dto;

import jakarta.validation.constraints.NotBlank;

public record LaboratoryInsertDTO(@NotBlank String name) {}
