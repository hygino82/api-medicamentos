package com.dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dev.hygino.entities.Laboratory;

public interface LaboratoryRepository extends JpaRepository<Laboratory, Long> {

}
