package com.dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dev.hygino.entities.Medicine;

public interface MedicineRepository extends JpaRepository<Medicine, Long> {

}
